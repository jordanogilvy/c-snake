#pragma once
#include <vector>
#include <stdlib.h>
#include <functional>
#include "square.hpp"
#include "constants.h"
#include "SDL.h"
using namespace std;


class Snake
{
private:
	std::vector<SnakeSquare*> m_bodyParts;
	Direction m_currentDirection;
	int m_previousTailX;
	int m_previousTailY;
	std::function<void(void)> m_loseCallback;

public:
	Snake() : m_currentDirection{ Direction::left }, m_previousTailX{ 0 }, m_previousTailY{ 0 }
	{
		setToDefaultSizeAndPosition();
	}

	void update();
	void grow();
	void setToDefaultSizeAndPosition();
	SnakeSquare* getHeadSquare();
	// TODO: make this a friend function of SquareItem
	bool containsPosition(int x, int y);
	Direction getDirection() { return m_currentDirection; }
	void setDirection(Direction direction) { m_currentDirection = direction; }
	void setLoseCallback(std::function<void(void)> callback) { m_loseCallback = callback; }
};

