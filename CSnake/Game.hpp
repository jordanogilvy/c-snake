#pragma once
#include <iostream>
#include "gamestate.hpp"

class Game
{
public:
	Game();
	~Game();

	void init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);

	void handleEvents();
	void update();
	void render();
	void clean();

	bool isGameRunning() { return m_isGameRunning; }

private:
	bool m_isGameRunning;
	GameState m_gameState;
	SDL_Window *m_window;
	SDL_Renderer *m_renderer;

};

