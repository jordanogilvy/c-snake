#include "gamestate.hpp"
#include "snake.hpp"

GameState::GameState() : m_currentScore{ 0 }, m_highScore{ 0 }, m_isGameOver{ false }
{
	m_snake.setLoseCallback(std::bind(&GameState::onGameLost, this));
}

void GameState::setDirection(Direction direction) 
{ 
	Direction currentDirection = m_snake.getDirection();
	Direction oppositeDirection;
	switch (currentDirection)
	{
	case Direction::left:
		oppositeDirection = Direction::right; break;
	case Direction::right:
		oppositeDirection = Direction::left; break;
	case Direction::up:
		oppositeDirection = Direction::down; break;
	case Direction::down:
		oppositeDirection = Direction::up;
	default:
		break;
	}

	if (direction != oppositeDirection)
	{
		m_snake.setDirection(direction);
	}
}

void GameState::update()
{
	m_snake.update();
	if (!m_isGameOver)
	{
		checkForFoodEaten();
	}
	else
	{
		restart();
	}
}

void GameState::checkForFoodEaten()
{
	SnakeSquare *snakeHead = m_snake.getHeadSquare();
	if (SquareItem::samePosition(m_currentFood, *snakeHead))
	{
		m_snake.grow();
		createNewFood();
		m_currentScore++;
		std::cout << "score: " << m_currentScore << std::endl;
	}
}

void GameState::restart()
{
	m_snake.setToDefaultSizeAndPosition();
	createNewFood();
	if (m_currentScore > m_highScore) 
	{
		m_highScore = m_currentScore;
	}
	m_currentScore = 0;
	m_isGameOver = false;
}

void GameState::createNewFood()
{
	std::cout << " make a new food !" << std::endl;

	int rx, ry;
	do
	{
		rx = rand() % gridWidth;
		ry = rand() % gridHeight;
	} while (m_snake.containsPosition(rx, ry));

	m_currentFood.setPosition(rx, ry);
}

void GameState::onGameLost()
{
	m_isGameOver = true;
}
