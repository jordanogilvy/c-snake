#pragma once
#include "snake.hpp"


class GameState
{
private:
	FoodSquare m_currentFood;
	int m_currentScore;
	int m_highScore;
	bool m_isGameOver;
	Snake m_snake;

public:
	GameState();

	void setDirection(Direction direction);

	void update();

	void checkForFoodEaten();

	void restart();

	void createNewFood();

	void onGameLost();

	int getCurrentScore() { return m_currentScore; }
	int getHighScore() { return m_highScore; }
};