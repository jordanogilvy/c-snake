#include "Game.hpp"
#include <string>

Game::Game()
{
}
Game::~Game()
{}

void Game::init(const char *title, int xpos, int ypos, int width, int height, bool fullscreen)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		int flags = fullscreen ? SDL_WINDOW_FULLSCREEN : 0;

		std::cout << "Successfully initialised SDL Subsystems..." << std::endl;

		m_window = SDL_CreateWindow(title, xpos, ypos, width, height, flags);
		if (m_window)
		{
			std::cout << "Window created successfully..." << std::endl;
		}

		m_renderer = SDL_CreateRenderer(m_window, -1, 0);
		if (m_renderer)
		{
			SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
			std::cout << "Renderer created successfully...";
		}

		m_isGameRunning = true;
	}
	else
	{
		m_isGameRunning = false;
	}
}

void Game::handleEvents()
{
	SDL_Event event;

	SDL_PollEvent(&event);

	switch (event.type) 
	{
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) 
			{
				case SDLK_LEFT:
					m_gameState.setDirection(Direction::left);
					break;
				case SDLK_RIGHT:
					m_gameState.setDirection(Direction::right);
					break;
				case SDLK_UP:
					m_gameState.setDirection(Direction::up);
					break;
				case SDLK_DOWN:
					m_gameState.setDirection(Direction::down);
					break;
				default:
					break;
			}
			break;
		case SDL_QUIT:
			m_isGameRunning = false;
			break;
		default:
			break;
	}
}

void Game::update()
{
	m_gameState.update();
	std::string current = std::to_string(m_gameState.getCurrentScore());
	std::string high = std::to_string(m_gameState.getHighScore());
	std::string title = "Score: " + current + ", best: " + high;
	SDL_SetWindowTitle(m_window, title.c_str());
}

void Game::render()
{
	SDL_RenderClear(m_renderer);

	SquareItem::renderAll(m_renderer);

	SDL_RenderPresent(m_renderer);
}

void Game::clean()
{
	SDL_DestroyWindow(m_window);
	SDL_DestroyRenderer(m_renderer);
	SDL_Quit();
	std::cout << "Game resources cleaned" << std::endl;
}
