#pragma once
const int gridWidth = 24;
const int gridHeight = 24;
const int snakeStartLength = 6;
const int windowWidth = 640;
const int windowHeight = 640;
// Our snake game will happen in a grid of squares. (0,0) is the top left corner of the grid.
enum Direction { up, down, left, right };
enum Color { black, white, purple, pink };
