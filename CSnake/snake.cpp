#include "snake.hpp"
#include "square.hpp"
#include <iostream>

void Snake::update()
{
	SnakeSquare *tail = m_bodyParts.back();
	m_previousTailX = tail->getX();
	m_previousTailY = tail->getY();

	SnakeSquare *head = getHeadSquare();

	int dx = 0; int dy = 0; int newX = 0; int newY = 0;
	switch (m_currentDirection)
	{
	case Direction::left:
		dx = -1;
		break;
	case Direction::right:
		dx = 1;
		break;
	case Direction::up:
		dy = -1;
		break;
	case Direction::down:
		dy = 1;
		break;
	}

	// if the new head position falls outside the grid, wrap around.
	newX = head->getX() + dx;
	newY = head->getY() + dy;
	if (newX < 0) newX = gridWidth - 1;
	else if (newX >= gridWidth) newX = 0;
	if (newY < 0) newY = gridHeight - 1;
	else if (newY >= gridHeight) newY = 0;

	//std::cout << "move tail to " << newX << " " << newY << std::endl;

	
	if (containsPosition(newX, newY))
	{
		if (m_loseCallback) {
			m_loseCallback();
		}
		std::cout << "you lose!" << std::endl;
	}

	tail->setPosition(newX, newY);
	std::rotate(m_bodyParts.rbegin(), m_bodyParts.rbegin() + 1, m_bodyParts.rend());
}

void Snake::grow()
{
	SnakeSquare *newPart = new SnakeSquare(m_previousTailX, m_previousTailY);
	m_bodyParts.push_back(newPart);
	std::cout << "grow the snake!" << std::endl;
}

void Snake::setToDefaultSizeAndPosition()
{
	while (!m_bodyParts.empty())
	{
		delete m_bodyParts.back();
		m_bodyParts.pop_back();
	}


	int startX = gridWidth / 2 - snakeStartLength / 2;
	int startY = gridHeight / 2;
	int x;

	for (int i = 0; i < snakeStartLength; i++)
	{
		x = startX + i;
		SnakeSquare *newPart = new SnakeSquare(i, startY);
		m_bodyParts.push_back(newPart);
	}

	setDirection(Direction::left);
}

SnakeSquare* Snake::getHeadSquare()
{
	return m_bodyParts.front();
}

bool Snake::containsPosition(int x, int y)
{
	for (auto const square : m_bodyParts)
	{
		if (square->getX() == x && square->getY() == y) return true;
	}

	return false;
}

