#pragma once

#include "constants.h"
#include <SDL.h>
#include <set>
#include <iostream>

class SquareItem
{
private:
	Color m_color;
	int m_x;
	int m_y;
	int m_id;
	void render(SDL_Renderer *renderer);
	static std::set<SquareItem*> s_allSquareItems;
	static int s_squareCount;

public:
	SquareItem();
	SquareItem(int x, int y);
	SquareItem(int x, int y, Color color);
	virtual ~SquareItem();

	void setPosition(int x, int y);
	int getX() const { return m_x; }
	int getY() const { return m_y; }
	void setColor(Color color) { m_color = color; }
	Color getColor() const { return m_color; }

	static void renderAll(SDL_Renderer *renderer);
	static bool samePosition(const SquareItem &squareA, const SquareItem &squareB);
};

class FoodSquare : public SquareItem
{
	using SquareItem::SquareItem;
public:
	FoodSquare() : SquareItem()
	{
		setColor(Color::purple);
	}
};

class SnakeSquare : public SquareItem
{
	using SquareItem::SquareItem;
};