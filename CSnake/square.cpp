#include "square.hpp"
#include "constants.h"
#include "SDL.h"
#include <iostream>


std::set<SquareItem*> SquareItem::s_allSquareItems = {};

int SquareItem::s_squareCount = 0;

void SquareItem::render(SDL_Renderer *renderer)
{
	SDL_Rect rect;
	rect.w = windowWidth / gridWidth;
	rect.h = windowHeight / gridHeight;
	rect.x = m_x * rect.w;
	rect.y = m_y * rect.h;	

	//std::cout << "render #" << m_id << " at " << rect.x << " " << rect.y <<  "from" << m_x << " " << m_y << std::endl;

	Uint8 r{ 0 };
	Uint8 g{ 0 };
	Uint8 b{ 0 };
	Uint8 a{ 0 };
	int getCode = SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
	if (getCode == 0)
	{
		switch (m_color)
		{
		case Color::white:
			SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
			break;
		case Color::pink:
			SDL_SetRenderDrawColor(renderer, 255, 192, 203, 255);
			break;
		case Color::purple:
			SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);
			break;
		case Color::black:
		default:
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			break;
		}
		
		SDL_RenderFillRect(renderer, &rect);

		SDL_SetRenderDrawColor(renderer, r, g, b, a);
	}
}


SquareItem::SquareItem() : SquareItem(0, 0, Color::white)
{
}

SquareItem::SquareItem(int x, int y) : SquareItem(x, y, Color::white)
{
}

SquareItem::SquareItem(int x, int y, Color color) : m_x{ x }, m_y{ y }, m_color{ color }
{
	s_allSquareItems.insert(this);
	m_id = s_squareCount++;
	//std::cout << "create square item #" << m_id << std::endl;
}

SquareItem::~SquareItem()
{
	s_allSquareItems.erase(this);
}


void SquareItem::renderAll(SDL_Renderer *renderer)
{
	for (auto square : s_allSquareItems)
	{
		square->render(renderer);
	}
}

void SquareItem::setPosition(int x, int y)
{
	m_x = x;
	m_y = y;
}

bool SquareItem::samePosition(const SquareItem &squareA, const SquareItem &squareB)
{
	return squareA.m_x == squareB.m_x && squareA.m_y == squareB.m_y;
}
