#include "constants.h"
#include "SDL.h"
#include "Game.hpp"
#include "gamestate.hpp"
#include "snake.hpp"

Game *game = nullptr;

int main(int argc, char *args[])
{
	const int fps = 10;
	const int frameDelay = 1000 / fps;
	Uint32 frameStart;
	int frameTime;
	game = new Game();
	game->init("Snake in C++", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight, false);

	while (game->isGameRunning())
	{
		frameStart = SDL_GetTicks();

		game->handleEvents();
		game->update();
		game->render();

		frameTime = SDL_GetTicks() - frameStart;

		if (frameDelay > frameTime)
		{
			SDL_Delay(frameDelay - frameTime);
		}
	}

	game->clean();

	return 0;

}